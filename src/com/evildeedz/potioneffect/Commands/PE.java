package com.evildeedz.potioneffect.Commands;

import java.util.Arrays;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.evildeedz.potioneffect.Main;

import net.md_5.bungee.api.ChatColor;

public class PE implements CommandExecutor{

	private static Main main;
	
	public PE(Main main)
	{
		PE.main = main;
	}


	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		// if sender isn't console
		if(sender instanceof Player)
		{
			Player player = (Player) sender;
			UUID pid = player.getUniqueId();
			// if player does have the permissions
			if(player.hasPermission("pe.use") || player.isOp())
			{
				// creating player data
				if(!main.getConfig().isConfigurationSection(player.getUniqueId().toString()))
				{
					main.getConfig().createSection(player.getUniqueId().toString());
					main.getConfig().createSection(player.getUniqueId().toString() + ".haste2");
					main.getConfig().set(player.getUniqueId().toString() + ".haste2", "false");
					main.getConfig().createSection(player.getUniqueId().toString() + ".waterbreathing");
					main.getConfig().set(player.getUniqueId().toString() + ".waterbreathing", "false");
					main.getConfig().createSection(player.getUniqueId().toString() + ".fireresistance");
					main.getConfig().set(player.getUniqueId().toString() + ".fireresistance", "false");
					main.getConfig().createSection(player.getUniqueId().toString() + ".nightvision");
					main.getConfig().set(player.getUniqueId().toString() + ".nightvision", "false");
					main.getConfig().createSection(player.getUniqueId().toString() + ".strength2");
					main.getConfig().set(player.getUniqueId().toString() + ".strength2", "false");
					main.getConfig().createSection(player.getUniqueId().toString() + ".inuse");
					main.getConfig().set(player.getUniqueId().toString() + ".inuse", 0);
					main.saveConfig();
				}
				
				main.setInventory(pid, Bukkit.createInventory(player, 54, ChatColor.AQUA.toString()+ ChatColor.BOLD + "Potion Selection"));
				
				// Disable All Enchants Button
				ItemStack barrier = new ItemStack(Material.BARRIER);
				ItemMeta barrierMeta = barrier.getItemMeta();	
				barrierMeta.setDisplayName(ChatColor.RED.toString()+ChatColor.BOLD + "Remove Potion Effects");
				barrier.setItemMeta(barrierMeta);
				
				// Haste
				ItemStack diamondPickaxe = new ItemStack(Material.DIAMOND_PICKAXE);
				ItemMeta diamondPickaxeMeta = diamondPickaxe.getItemMeta();
				diamondPickaxeMeta.setDisplayName(ChatColor.YELLOW + "Haste 2");
				if(main.getConfig().getString(player.getUniqueId().toString()+".haste2").equals("false"))
				{
					String[] lore = new String[] {" ", ChatColor.GREEN + "Click to Enable!"};
					diamondPickaxeMeta.setLore(Arrays.asList(lore));
				}
				else
				{
					String[] lore = new String[] {" ", ChatColor.RED + "Click to Disable!"};
					diamondPickaxeMeta.setLore(Arrays.asList(lore));
					diamondPickaxeMeta.addEnchant(Enchantment.ARROW_INFINITE, 1, false);
					diamondPickaxeMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
				}
				diamondPickaxeMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
				diamondPickaxe.setItemMeta(diamondPickaxeMeta);
					
				// Water Breathing
				ItemStack pufferFish = new ItemStack(Material.PUFFERFISH);
				ItemMeta pufferFishMeta = pufferFish.getItemMeta();
				pufferFishMeta.setDisplayName(ChatColor.YELLOW + "Water Breathing");
				if(main.getConfig().getString(player.getUniqueId().toString()+".waterbreathing").equals("false"))
				{
					String[] lore = new String[] {" ", ChatColor.GREEN + "Click to Enable!"};
					pufferFishMeta.setLore(Arrays.asList(lore));
				}
				else
				{
					String[] lore = new String[] {" ", ChatColor.RED + "Click to Disable!"};
					pufferFishMeta.setLore(Arrays.asList(lore));
					pufferFishMeta.addEnchant(Enchantment.ARROW_INFINITE, 1, false);
					pufferFishMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
				}
				pufferFish.setItemMeta(pufferFishMeta);
				
				// Fire Resistance
				ItemStack fireball = new ItemStack(Material.FIRE_CHARGE);
				ItemMeta fireballMeta = fireball.getItemMeta();
				fireballMeta.setDisplayName(ChatColor.YELLOW + "Fire Resistance");
				if(main.getConfig().getString(player.getUniqueId().toString()+".fireresistance").equals("false"))
				{
					String[] lore = new String[] {" ", ChatColor.GREEN + "Click to Enable!"};
					fireballMeta.setLore(Arrays.asList(lore));
				}
				else
				{
					String[] lore = new String[] {" ", ChatColor.RED + "Click to Disable!"};
					fireballMeta.setLore(Arrays.asList(lore));
					fireballMeta.addEnchant(Enchantment.ARROW_INFINITE, 1, false);
					fireballMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
				}
				fireball.setItemMeta(fireballMeta);
				
				// Night Vision
				ItemStack eyeofender = new ItemStack(Material.ENDER_EYE);
				ItemMeta eyeofendermeta = eyeofender.getItemMeta();
				eyeofendermeta.setDisplayName(ChatColor.YELLOW + "Night Vision");
				if(main.getConfig().getString(player.getUniqueId().toString()+".nightvision").equals("false"))
				{
					String[] lore = new String[] {" ", ChatColor.GREEN + "Click to Enable!"};
					eyeofendermeta.setLore(Arrays.asList(lore));
				}
				else
				{
					String[] lore = new String[] {" ", ChatColor.RED + "Click to Disable!"};
					eyeofendermeta.setLore(Arrays.asList(lore));
					eyeofendermeta.addEnchant(Enchantment.ARROW_INFINITE, 1, false);
					eyeofendermeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
				}
				eyeofender.setItemMeta(eyeofendermeta);
				
				// Strength 2
				ItemStack goldenApple = new ItemStack(Material.GOLDEN_APPLE);
				ItemMeta goldenAppleMeta = goldenApple.getItemMeta();
				goldenAppleMeta.setDisplayName(ChatColor.YELLOW + "Strength 2");
				if(main.getConfig().getString(player.getUniqueId().toString()+".strength2").equals("false"))
				{
					String[] lore = new String[] {" ", ChatColor.GREEN + "Click to Enable!"};
					goldenAppleMeta.setLore(Arrays.asList(lore));
				}
				else
				{
					String[] lore = new String[] {" ", ChatColor.RED + "Click to Disable!"};
					goldenAppleMeta.setLore(Arrays.asList(lore));
					goldenAppleMeta.addEnchant(Enchantment.ARROW_INFINITE, 1, false);
					goldenAppleMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
				}
				goldenApple.setItemMeta(goldenAppleMeta);
				
				// Decoration
				ItemStack decoration = new ItemStack(Material.PURPLE_STAINED_GLASS_PANE);
				ItemMeta decorationMeta = decoration.getItemMeta();
				decorationMeta.setDisplayName(ChatColor.BLUE.toString());
				decorationMeta.addEnchant(Enchantment.ARROW_INFINITE, 1, false);
				decorationMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
				decorationMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
				decoration.setItemMeta(decorationMeta);
				
				// Adding to GUI
				main.getGui(pid).setItem(4, barrier);
				main.getGui(pid).setItem(20, diamondPickaxe);
				main.getGui(pid).setItem(22, pufferFish);
				main.getGui(pid).setItem(24, fireball);
				main.getGui(pid).setItem(30, eyeofender);
				main.getGui(pid).setItem(32, goldenApple);
				for(int i =1;i<=9;i++)
				{
					if(i-1!=4)
						main.getGui(pid).setItem(i-1, decoration);
				}
				for(int i = 46;i<=54;i++)
				{
					main.getGui(pid).setItem(i-1, decoration);
				}
				// Opening GUI
				player.openInventory(main.getGui(pid));
			}
			else
			{
				player.sendMessage(ChatColor.DARK_RED+ "Insufficient Permissions!");
			}
			
		}
		else
		{
			System.out.println("Can only be run from a console");
		}
		return false;
	}
}

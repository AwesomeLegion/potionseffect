package com.evildeedz.potioneffect.Listeners;

import java.util.Arrays;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.evildeedz.potioneffect.Main;

import net.md_5.bungee.api.ChatColor;


public class ClickListener implements Listener{

	private static Main main;
	
	public ClickListener(Main main)
	{
		ClickListener.main = main;
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent e)
	{
		Player player = (Player)e.getWhoClicked();
		UUID pid = player.getUniqueId();
		// Item Clicked in the Inventory
		if(e.getInventory() == main.getGui(pid))
		{
			e.setCancelled(true);
			
			// Max amout by permission
			int maxAmount = 5;
			int currentAmount = main.getConfig().getInt(player.getUniqueId().toString()+".inuse");
			
			
			for(int i = 1; i <= 5; i++)
			{
				if(player.hasPermission("pe.maxamount."+i))
				{
					maxAmount = i;
				}
			}
			
			if(e.isLeftClick())
			{
				ItemStack clickedItem = e.getCurrentItem();
				
				// Remove All Potions
				if(clickedItem.getItemMeta().getDisplayName().contains("Remove Potion Effects"))
				{
					removeAllEffects(player);
					player.sendMessage(ChatColor.RED +"All Effects Removed!");
					player.closeInventory();
				}
				// Haste 2
				else if(clickedItem.getItemMeta().getDisplayName().contains("Haste 2"))
				{
					if(main.getConfig().getString(player.getUniqueId().toString() + ".haste2").equals("false"))
					{
						// If Max amount not reached
						if(currentAmount < maxAmount)
						{
							player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.MAX_VALUE, 1));
							player.sendMessage(ChatColor.YELLOW+"Haste 2" + ChatColor.GREEN +" Added!");
							main.getConfig().set(player.getUniqueId().toString()+".inuse", currentAmount+1);
							main.getConfig().set(player.getUniqueId().toString()+".haste2", "true");
							
							ItemMeta currentMeta = e.getCurrentItem().getItemMeta();
							String[] lore = new String[] {" ", ChatColor.RED + "Click to Disable!"};
							currentMeta.setLore(Arrays.asList(lore));
							currentMeta.addEnchant(Enchantment.ARROW_INFINITE, 1, false);
							currentMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
							e.getCurrentItem().setItemMeta(currentMeta);
						}
						else
							player.sendMessage(ChatColor.RED + "Already at the Max Potion Effects limit!");
					}
					else
					{
						player.removePotionEffect(PotionEffectType.FAST_DIGGING);
						player.sendMessage(ChatColor.YELLOW+"Haste 2" + ChatColor.RED +" Removed!");
						main.getConfig().set(player.getUniqueId().toString()+".inuse", currentAmount-1);
						main.getConfig().set(player.getUniqueId().toString()+".haste2", "false");
						
						ItemMeta currentMeta = e.getCurrentItem().getItemMeta();
						String[] lore = new String[] {" ", ChatColor.GREEN + "Click to Enable!"};
						currentMeta.setLore(Arrays.asList(lore));
						currentMeta.removeEnchant(Enchantment.ARROW_INFINITE);
						e.getCurrentItem().setItemMeta(currentMeta);
					}
				}
				// Water Breathing
				else if(clickedItem.getItemMeta().getDisplayName().contains("Water Breathing"))
				{
					if(main.getConfig().getString(player.getUniqueId().toString() + ".waterbreathing").equals("false"))
					{
						// If Max amount not reached
						if(currentAmount < maxAmount)
						{
							player.addPotionEffect(new PotionEffect(PotionEffectType.WATER_BREATHING, Integer.MAX_VALUE, 0));
							player.sendMessage(ChatColor.YELLOW+"Water Breathing" + ChatColor.GREEN +" Added!");
							main.getConfig().set(player.getUniqueId().toString()+".inuse", currentAmount+1);
							main.getConfig().set(player.getUniqueId().toString()+".waterbreathing", "true");

							ItemMeta currentMeta = e.getCurrentItem().getItemMeta();
							String[] lore = new String[] {" ", ChatColor.RED + "Click to Disable!"};
							currentMeta.setLore(Arrays.asList(lore));
							currentMeta.addEnchant(Enchantment.ARROW_INFINITE, 1, false);
							currentMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
							e.getCurrentItem().setItemMeta(currentMeta);
						}
						else
							player.sendMessage(ChatColor.RED + "Already at the Max Potion Effects assigned!");
					}
					else
					{
						player.removePotionEffect(PotionEffectType.WATER_BREATHING);
						player.sendMessage(ChatColor.YELLOW+"Water Breathing" + ChatColor.RED +" Removed!");
						main.getConfig().set(player.getUniqueId().toString()+".inuse", currentAmount-1);
						main.getConfig().set(player.getUniqueId().toString()+".waterbreathing", "false");
						
						ItemMeta currentMeta = e.getCurrentItem().getItemMeta();
						String[] lore = new String[] {" ", ChatColor.GREEN + "Click to Enable!"};
						currentMeta.setLore(Arrays.asList(lore));
						currentMeta.removeEnchant(Enchantment.ARROW_INFINITE);
						e.getCurrentItem().setItemMeta(currentMeta);
					}
				}
				// Fire Resistance
				else if(clickedItem.getItemMeta().getDisplayName().contains("Fire Resistance"))
				{
					if(main.getConfig().getString(player.getUniqueId().toString() + ".fireresistance").equals("false"))
					{
						// If Max amount not reached
						if(currentAmount < maxAmount)
						{
							player.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, Integer.MAX_VALUE, 0));
							player.sendMessage(ChatColor.YELLOW+"Fire Resistance" + ChatColor.GREEN +" Added!");
							main.getConfig().set(player.getUniqueId().toString()+".inuse", currentAmount+1);
							main.getConfig().set(player.getUniqueId().toString()+".fireresistance", "true");

							ItemMeta currentMeta = e.getCurrentItem().getItemMeta();
							String[] lore = new String[] {" ", ChatColor.RED + "Click to Disable!"};
							currentMeta.setLore(Arrays.asList(lore));
							currentMeta.addEnchant(Enchantment.ARROW_INFINITE, 1, false);
							currentMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
							e.getCurrentItem().setItemMeta(currentMeta);
						}
						else
							player.sendMessage(ChatColor.RED + "Already at the Max Potion Effects limit!");
					}
					else
					{
						player.removePotionEffect(PotionEffectType.FIRE_RESISTANCE);
						player.sendMessage(ChatColor.YELLOW+"Fire Resistance" + ChatColor.RED +" Removed!");
						main.getConfig().set(player.getUniqueId().toString()+".inuse", currentAmount-1);
						main.getConfig().set(player.getUniqueId().toString()+".fireresistance", "false");
						
						ItemMeta currentMeta = e.getCurrentItem().getItemMeta();
						String[] lore = new String[] {" ", ChatColor.GREEN + "Click to Enable!"};
						currentMeta.setLore(Arrays.asList(lore));
						currentMeta.removeEnchant(Enchantment.ARROW_INFINITE);
						e.getCurrentItem().setItemMeta(currentMeta);
					}
				}
				// Night Vision
				else if(clickedItem.getItemMeta().getDisplayName().contains("Night Vision"))
				{
					if(main.getConfig().getString(player.getUniqueId().toString() + ".nightvision").equals("false"))
					{
						// If Max amount not reached
						if(currentAmount < maxAmount)
						{
							player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0));
							player.sendMessage(ChatColor.YELLOW+"Night Vision" + ChatColor.GREEN +" Added!");
							main.getConfig().set(player.getUniqueId().toString()+".inuse", currentAmount+1);
							main.getConfig().set(player.getUniqueId().toString()+".nightvision", "true");

							ItemMeta currentMeta = e.getCurrentItem().getItemMeta();
							String[] lore = new String[] {" ", ChatColor.RED + "Click to Disable!"};
							currentMeta.setLore(Arrays.asList(lore));
							currentMeta.addEnchant(Enchantment.ARROW_INFINITE, 1, false);
							currentMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
							e.getCurrentItem().setItemMeta(currentMeta);
						}
						else
							player.sendMessage(ChatColor.RED + "Already at the Max Potion Effects limit!");
					}
					else
					{
						player.removePotionEffect(PotionEffectType.NIGHT_VISION);
						player.sendMessage(ChatColor.YELLOW+"Night Vision" + ChatColor.RED +" Removed!");
						main.getConfig().set(player.getUniqueId().toString()+".inuse", currentAmount-1);
						main.getConfig().set(player.getUniqueId().toString()+".nightvision", "false");
						
						ItemMeta currentMeta = e.getCurrentItem().getItemMeta();
						String[] lore = new String[] {" ", ChatColor.GREEN + "Click to Enable!"};
						currentMeta.setLore(Arrays.asList(lore));
						currentMeta.removeEnchant(Enchantment.ARROW_INFINITE);
						e.getCurrentItem().setItemMeta(currentMeta);
					}
				}
				// Strength 2
				else if(clickedItem.getItemMeta().getDisplayName().contains("Strength 2"))
				{
					if(main.getConfig().getString(player.getUniqueId().toString() + ".strength2").equals("false"))
					{
						// If Max amount not reached
						if(currentAmount < maxAmount)
						{
							player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 1));
							player.sendMessage(ChatColor.YELLOW+"Strength 2" + ChatColor.GREEN +" Added!");
							main.getConfig().set(player.getUniqueId().toString()+".inuse", currentAmount+1);
							main.getConfig().set(player.getUniqueId().toString()+".strength2", "true");

							ItemMeta currentMeta = e.getCurrentItem().getItemMeta();
							String[] lore = new String[] {" ", ChatColor.RED + "Click to Disable!"};
							currentMeta.setLore(Arrays.asList(lore));
							currentMeta.addEnchant(Enchantment.ARROW_INFINITE, 1, false);
							currentMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
							e.getCurrentItem().setItemMeta(currentMeta);
						}
						else
							player.sendMessage(ChatColor.RED + "Already at the Max Potion Effects limit!");
					}
					else
					{
						player.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
						player.sendMessage(ChatColor.YELLOW+"Strength 2" + ChatColor.RED +" Removed!");
						main.getConfig().set(player.getUniqueId().toString()+".inuse", currentAmount-1);
						main.getConfig().set(player.getUniqueId().toString()+".strength2", "false");
						
						ItemMeta currentMeta = e.getCurrentItem().getItemMeta();
						String[] lore = new String[] {" ", ChatColor.GREEN + "Click to Enable!"};
						currentMeta.setLore(Arrays.asList(lore));
						currentMeta.removeEnchant(Enchantment.ARROW_INFINITE);
						e.getCurrentItem().setItemMeta(currentMeta);
					}
				}
				main.saveConfig();
			}
		}
	}
	
	@EventHandler
	public void onMilk(PlayerItemConsumeEvent e)
	{
		Player player = e.getPlayer();
		if(e.getItem().getType() == Material.MILK_BUCKET && player.hasPermission("pe.use"))
		{
			removeAllEffects(player);
		}
	}
	
	@EventHandler
	public void onDrag(InventoryDragEvent e)
	{
		if(e.getInventory() == main.getGui(((Player) e.getWhoClicked()).getUniqueId()))
		{
			e.setCancelled(true);
		}
	}
	
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e)
	{
		Player player = e.getPlayer();
		if(player.hasPermission("pe.use"))
		{
			addExistingEffects(player);
		}
	}
	
	
	@EventHandler
	public void onRespawn(PlayerRespawnEvent e)
	{
		Player player = e.getPlayer();
		if(player.hasPermission("pe.use"))
			removeAllEffects(player);
	}

	
	public void removeAllEffects(Player player)
	{
		player.removePotionEffect(PotionEffectType.FAST_DIGGING);
		main.getConfig().set(player.getUniqueId().toString()+".haste2", "false");
		player.removePotionEffect(PotionEffectType.WATER_BREATHING);
		main.getConfig().set(player.getUniqueId().toString()+".waterbreathing", "false");
		player.removePotionEffect(PotionEffectType.FIRE_RESISTANCE);
		main.getConfig().set(player.getUniqueId().toString()+".fireresistance", "false");
		player.removePotionEffect(PotionEffectType.NIGHT_VISION);
		main.getConfig().set(player.getUniqueId().toString()+".nightvision", "false");
		player.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
		main.getConfig().set(player.getUniqueId().toString()+".strength2", "false");
		main.getConfig().set(player.getUniqueId().toString() + ".inuse", 0);
		main.saveConfig();
	}
	
	public void addExistingEffects(Player player)
	{
		if(main.getConfig().isConfigurationSection(player.getUniqueId().toString()))
		{			
			if(main.getConfig().getString(player.getUniqueId().toString()+".haste2").equals("true"))
				player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.MAX_VALUE, 1));
			
			if(main.getConfig().getString(player.getUniqueId().toString()+".waterbreathing").equals("true"))
				player.addPotionEffect(new PotionEffect(PotionEffectType.WATER_BREATHING, Integer.MAX_VALUE, 0));
			
			if(main.getConfig().getString(player.getUniqueId().toString()+".fireresistance").equals("true"))
				player.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, Integer.MAX_VALUE, 0));
			
			if(main.getConfig().getString(player.getUniqueId().toString()+".nightvision").equals("true"))
				player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0));
			
			if(main.getConfig().getString(player.getUniqueId().toString()+".strength2").equals("true"))
				player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 1));
			
			main.saveConfig();
		}
	}
}

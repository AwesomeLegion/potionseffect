package com.evildeedz.potioneffect;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;

import com.evildeedz.potioneffect.Commands.PE;
import com.evildeedz.potioneffect.Listeners.ClickListener;

public class Main extends JavaPlugin{

	private HashMap<UUID, Inventory> inventory;
	
	@Override
	public void onEnable()
	{
		
		inventory  = new HashMap<>();
		
		// Config
		getConfig().options().copyDefaults();
		saveDefaultConfig();
		
		// Commands
		getCommand("potioneffects").setExecutor(new PE(this));
		getCommand("pe").setExecutor(new PE(this));
		
		// Events
		Bukkit.getPluginManager().registerEvents(new ClickListener(this), this);
		
	}
	
	public Inventory getGui(UUID uuid) { return inventory.get(uuid); }
	public void setInventory(UUID uuid, Inventory inv) { inventory.put(uuid, inv); }
	
}
